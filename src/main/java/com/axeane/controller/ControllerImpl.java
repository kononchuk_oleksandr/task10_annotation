package com.axeane.controller;

import com.axeane.model.Logic;
import com.axeane.model.LogicImpl;

public class ControllerImpl implements Controller {
  private Logic logic = new LogicImpl();

  @Override
  public void taskTwoAndThree() {
    logic.taskTwoAndThree();
  }

  @Override
  public void taskFour() {
    logic.taskFour();
  }

  @Override
  public void taskSix() {
    logic.taskSix();
  }

  @Override
  public void taskSeven() {
    logic.taskSeven();
  }
}
