package com.axeane.model;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Information {
  private static Logger logger = LogManager.getLogger(Information.class);

  public void getInformation(Object obj) {
    Class cls = obj.getClass();
    Class[] classArray = cls.getDeclaredClasses();
    Constructor[] constructors = cls.getDeclaredConstructors();
    Method[] methods = cls.getDeclaredMethods();
    Field[] fields = cls.getDeclaredFields();
    Annotation[] annotations = cls.getDeclaredAnnotations();

    logger.info("Class name is: " + cls.getName());
    logger.info("Class simple name is: " + cls.getSimpleName());

    logger.info("Class classes: ");
    if (classArray.length > 0) {
      for (Class c : classArray) {
        logger.info("\t" + c.getModifiers() + " " + c.getName());
      }
    } else {
      logger.info("\tnone");
    }


    logger.info("Class constructors: ");
    if (constructors.length > 0) {
      for (Constructor constructor : constructors) {
        logger.info("\t" + constructor.getName());
      }
    } else {
      logger.info("\tnone");
    }


    logger.info("Class methods: ");
    if (methods.length > 0) {
      for (Method method : methods) {
        logger.info("\t" + method.getModifiers() + " " + method.getReturnType() + " " + method.getName());
      }
    } else {
      logger.info("\tnone");
    }


    logger.info("Class fields: ");
    if (fields.length > 0) {
      for (Field field : fields) {
        logger.info("\t" + field.getType() + " " + field.getName());
      }
    } else {
      logger.info("\tnone");
    }


    logger.info("Class annotations: ");
    if (annotations.length > 0) {
      for (Annotation annotation : annotations) {
        logger.info("\t" + annotation.annotationType().getAnnotations());
      }
    } else {
      logger.info("\tnone");
    }
  }
}
