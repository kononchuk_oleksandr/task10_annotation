package com.axeane.model;

public interface Logic {

  /**
   * TASK 2, 3:
   * Create your own annotation. Create class with a few fields,
   * some of which annotate with this annotation. Through reflection
   * print those fields in the class that were annotate by this annotation.
   * Print annotation value into console (e.g. @Annotation(name = "111"))
   */
  void taskTwoAndThree();

  /**
   * TASK 4:
   * Invoke method (three method with different parameters and return types).
   */
  void taskFour();

  /**
   * TASK 6:
   * Invoke myMethod(String a, int ... args) and myMethod(String … args).
   */
  void taskSix();

  /**
   * TASK 7:
   * Create your own class that received object of unknown type and show all
   * information about that Class.
   */
  void taskSeven();
}
