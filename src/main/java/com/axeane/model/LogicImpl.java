package com.axeane.model;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LogicImpl implements Logic {
  private static Logger logger = LogManager.getLogger(LogicImpl.class);


  public void taskTwoAndThree() {
    Field[] fields = TaskTwoAndThree.class.getDeclaredFields();
    for (Field field : fields) {
      if (field.isAnnotationPresent(MyOwnAnnotation.class)) {
        MyOwnAnnotation annotation = (MyOwnAnnotation) field.getAnnotation(MyOwnAnnotation.class);
        String someString = annotation.someString();
        int someInt = annotation.someInt();

        logger.info("Filed: " + field.getName());
        logger.info("\tsomeString in @MyOwnAnnotation is: " + someString);
        logger.info("\tsomeInt in @MyOwnAnnotation is: " + someInt);
      }
    }
  }

  public void taskFour() {
    Class<InvokeMethods> cls = InvokeMethods.class;
    try {
      Method methodOne = cls.getDeclaredMethod("getString", String.class);
      methodOne.setAccessible(true);
      logger.info("Method 1 - getString");
      logger.info("\t" + methodOne.invoke(new InvokeMethods(), "ALEX"));

      Method methodTwo = cls.getDeclaredMethod("getSqrt", int.class);
      methodTwo.setAccessible(true);
      logger.info("Method 2 - getSqrt");
      logger.info("\t" + methodTwo.invoke(new InvokeMethods(), 25));

      Method methodThree = cls.getDeclaredMethod("isEqual", int.class, int.class);
      methodThree.setAccessible(true);
      logger.info("Method 3 - isEqual");
      logger.info("\t" + methodThree.invoke(new InvokeMethods(), 10, 11));

    } catch (Exception e) {
      logger.error(e.getMessage());
    }
  }

  public void taskSix() {
    Class<InvokeMethods> cls = InvokeMethods.class;
    try {
      Method methodOne = cls.getDeclaredMethod("myMethod", String[].class);
      methodOne.setAccessible(true);
      String[] args = {"arg1", "arg2", "arg3"};
      logger.info("Method One: method with parameter (String ... args):");
      methodOne.invoke(new InvokeMethods(), (Object) args);

      Method methodTwo = cls.getDeclaredMethod("myMethod", String.class, int[].class);
      methodTwo.setAccessible(true);
      int[] salary = {1, 2, 3, 4, 5};
      logger.info("Method Two: method with parameters (String a, int ... args):");
      methodTwo.invoke(new InvokeMethods(), "Alex", salary);
    } catch (Exception e) {
      logger.error(e.getMessage());
    }
  }

  public void taskSeven() {
    new Information().getInformation(new NewModel("Alex"));
  }
}
