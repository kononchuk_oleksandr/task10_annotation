package com.axeane.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class InvokeMethods {
  private static Logger logger = LogManager.getLogger(InvokeMethods.class);

  private String getString(String str) {
    return str.toLowerCase();
  }

  private double getSqrt(int a) {
    return Math.sqrt(a);
  }

  private boolean isEqual(int a, int b) {
    return a == b;
  }

  private void myMethod(String a, int ... args) {
    for (int num : args) {
      logger.info("\t" + a + " salary after " + num + " month(s) is: " + (num * 200) + "$");
    }
  }

  private void myMethod(String ... args) {
    for (String str: args) {
      logger.info("\t" + str);
    }
  }
}
