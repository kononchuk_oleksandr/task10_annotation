package com.axeane.model;

public class TaskTwoAndThree {

  public TaskTwoAndThree() {
  }

  @MyOwnAnnotation(someString = "Alex")
  private String name;
  @MyOwnAnnotation(someInt = 28)
  private int age;
  @MyOwnAnnotation
  private String secondName;
  private int life;

}
