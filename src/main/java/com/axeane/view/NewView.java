package com.axeane.view;

import com.axeane.controller.Controller;
import com.axeane.controller.ControllerImpl;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class NewView {
  private static Logger logger = LogManager.getLogger(NewView.class);
  private Scanner scanner = new Scanner(System.in);
  private Controller controller = new ControllerImpl();
  private Map<String, String> menu;
  private Map<String, Printable> menuMethods;

  public NewView() {
    menu = new LinkedHashMap<>();
    menu.put("1", "1 - Task 2 and 3: Print annotation value.");
    menu.put("2", "2 - Task 4: Invoke three methods.");
    menu.put("3", "3 - Task 6: Invoke two different myMethod().");
    menu.put("4", "4 - Task 7: Show all information about class.");
    menu.put("E", "E - Exit");

    menuMethods = new LinkedHashMap<>();
    menuMethods.put("1", controller::taskTwoAndThree);
    menuMethods.put("2", controller::taskFour);
    menuMethods.put("3", controller::taskSix);
    menuMethods.put("4", controller::taskSeven);
  }

  private void menu() {
    menu.values().forEach(str -> logger.info(str));
  }

  public void start() {
    String key = null;
    do {
      menu();
      key = scanner.nextLine();
      try {
        menuMethods.get(key).print();
      } catch (Exception e) {
        e.getMessage();
      }
    } while (!key.equalsIgnoreCase("e"));
  }
}
