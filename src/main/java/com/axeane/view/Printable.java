package com.axeane.view;

@FunctionalInterface
public interface Printable {
  void print();
}
